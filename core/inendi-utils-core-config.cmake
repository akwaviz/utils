# - Try to find INENDI_UTILS_CORE

find_package(PkgConfig)

set(INENDI_UTILS_CORE_LIBRARIES ${INENDI_UTILS_CORE_LIBRARY} dl )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PVLOGGER_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(INENDI_UTILS_CORE DEFAULT_MSG
          INENDI_UTILS_CORE_LIBRARIES INENDI_UTILS_CORE_INCLUDE_DIR)

message(STATUS "INENDI_UTILS_CORE include dirs: ${INENDI_UTILS_CORE_INCLUDE_DIR}")
message(STATUS "INENDI_UTILS_CORE libraries: ${INENDI_UTILS_CORE_LIBRARIES}")

mark_as_advanced(INENDI_UTILS_CORE_INCLUDE_DIR INENDI_UTILS_CORE_LIBRARIES)
